#!/bin/bash

#one script plug and unplug the device

function link {
	from=$1
	ftype=$2
	fpad=$3
	to=$4
	ttype=$5
	tpad=$6
	ltype=$7
	mkdir -p "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}"
	echo $ltype > "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}/type"
	ln -s "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}" "/configfs/vimc/$DEV/${ftype}:${from}/source:${fpad}" 2>/dev/null
}

function stream {
	while :
	do
		media-ctl -d platform:vimc-000 -V "\"sen\":0[fmt:SBGGR8_1X8/640x480]"
		media-ctl -d platform:vimc-000 -V "\"deb\":0[fmt:SBGGR8_1X8/640x480]"
		media-ctl -d platform:vimc-000 -V "\"sca\":0[fmt:RGB888_1X24/640x480]"
		v4l2-ctl -z platform:vimc-000 -d "cap" -v pixelformat=RGB3
		echo "steaming"
		v4l2-ctl -z platform:vimc-000 -d "cap" --stream-mmap
	done
}

function rm_entity {
	ent=$1
	find /configfs -type l -name "*${ent}*" -delete
	find /configfs -type d -name "*${ent}*" -delete
	echo 0 > /configfs/vimc/$DEV/hotplug
	echo 1 > /configfs/vimc/$DEV/hotplug
	sleep 1
}

function recreate_sca {
	while :
	do
		rm_entity sca
		mkdir -p /configfs/vimc/$DEV/vimc-scaler:sca
		link deb vimc-debayer 1 sca vimc-scaler 0 on
		link sca vimc-scaler 1 cap vimc-capture 0 on
		echo 0 > /configfs/vimc/$DEV/hotplug
		echo 1 > /configfs/vimc/$DEV/hotplug
		sleep $((RANDOM % 3))
	done
}

function recreate_deb {
	while :
	do
		rm_entity deb
		mkdir -p /configfs/vimc/$DEV/vimc-debayer:deb
		link sen vimc-sensor 0 deb vimc-debayer 0 on
		link deb vimc-debayer 1 sca vimc-scaler 0 on
		echo 0 > /configfs/vimc/$DEV/hotplug
		echo 1 > /configfs/vimc/$DEV/hotplug
		sleep $((RANDOM % 3))
	done
}

function recreate_sen {
	while :
	do
		rm_entity sen
		mkdir -p /configfs/vimc/$DEV/vimc-sensor:sen
		link sen vimc-sensor 0 deb vimc-debayer 0 on
		echo 0 > /configfs/vimc/$DEV/hotplug
		echo 1 > /configfs/vimc/$DEV/hotplug
		sleep $((RANDOM % 3))
	done
}

function recreate_cap {
	while :
	do
		rm_entity cap
		mkdir -p /configfs/vimc/$DEV/vimc-capture:cap
		link sca vimc-scaler 1 cap vimc-capture 0 on
		echo 0 > /configfs/vimc/$DEV/hotplug
		echo 1 > /configfs/vimc/$DEV/hotplug
		sleep $((RANDOM % 3))
	done
}


function prepare {
DEV=mdev
modprobe -vr vimc
modprobe -v vimc sca_mult=1
mount -t configfs none /configfs
echo 15 > /proc/sys/kernel/printk
mkdir /configfs/vimc/$DEV

mkdir -p /configfs/vimc/$DEV/vimc-sensor:sen
mkdir -p /configfs/vimc/$DEV/vimc-debayer:deb
mkdir -p /configfs/vimc/$DEV/vimc-scaler:sca
mkdir -p /configfs/vimc/$DEV/vimc-capture:cap
link sen vimc-sensor 0 deb vimc-debayer 0 on
link deb vimc-debayer 1 sca vimc-scaler 0 on
link sca vimc-scaler 1 cap vimc-capture 0 on
}

function main2 {
prepare
stream &
while :
do
	echo disabled > /configfs/vimc/mdev/vimc-capture:cap/sink:0/sca-to-cap/type
	echo 0 > /configfs/vimc/$DEV/hotplug
	echo 1 > /configfs/vimc/$DEV/hotplug
	sleep 0.5
	echo enabled > /configfs/vimc/mdev/vimc-capture:cap/sink:0/sca-to-cap/type
	echo 0 > /configfs/vimc/$DEV/hotplug
	echo 1 > /configfs/vimc/$DEV/hotplug
	sleep 2

	echo disabled > /configfs/vimc/mdev/vimc-scaler:sca/sink:0/deb-to-sca/type
	echo 0 > /configfs/vimc/$DEV/hotplug
	echo 1 > /configfs/vimc/$DEV/hotplug
	sleep 0.5
	echo enabled > /configfs/vimc/mdev/vimc-scaler:sca/sink:0/deb-to-sca/type
	echo 0 > /configfs/vimc/$DEV/hotplug
	echo 1 > /configfs/vimc/$DEV/hotplug
	sleep 2

	echo disabled > /configfs/vimc/mdev/vimc-debayer:deb/sink:0/sen-to-deb/type
	echo 0 > /configfs/vimc/$DEV/hotplug
	echo 1 > /configfs/vimc/$DEV/hotplug
	sleep 0.5
	echo enabled > /configfs/vimc/mdev/vimc-debayer:deb/sink:0/sen-to-deb/type
	echo 0 > /configfs/vimc/$DEV/hotplug
	echo 1 > /configfs/vimc/$DEV/hotplug
	sleep 2
done
}

modprobe vimc
echo vimc* > /sys/kernel/debug/tracing/set_ftrace_filter
echo > /sys/kernel/debug/tracing/trace

prepare
stream &
recreate_sen &
recreate_deb &
recreate_sca &
recreate_cap &
