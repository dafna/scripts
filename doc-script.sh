# Creating the entities
mkdir "/configfs/vimc/mdev"
mkdir "/configfs/vimc/mdev/vimc-sensor:sen"
mkdir "/configfs/vimc/mdev/vimc-debayer:deb"
mkdir "/configfs/vimc/mdev/vimc-scaler:sca"
mkdir "/configfs/vimc/mdev/vimc-capture:cap-sca" #/dev/video2
mkdir "/configfs/vimc/mdev/vimc-capture:cap-sen" #/dev/video1
mkdir "/configfs/vimc/mdev/vimc-capture:cap-deb" #/dev/video0

# Creating the links
#sen -> deb
mkdir "/configfs/vimc/mdev/vimc-sensor:sen/source:0/to-deb"
ln -s "/configfs/vimc/mdev/vimc-debayer:deb/sink:0" "/configfs/vimc/mdev/vimc-sensor:sen/source:0/to-deb"
echo immutable > "/configfs/vimc/mdev/vimc-sensor:sen/source:0/to-deb/type"

#deb -> sca
mkdir "/configfs/vimc/mdev/vimc-debayer:deb/source:1/to-sca"
ln -s "/configfs/vimc/mdev/vimc-scaler:sca/sink:0" "/configfs/vimc/mdev/vimc-debayer:deb/source:1/to-sca"
echo immutable > "/configfs/vimc/mdev/vimc-debayer:deb/source:1/to-sca/type"

#sca -> cap-sca
mkdir "/configfs/vimc/mdev/vimc-scaler:sca/source:1/to-cap-sca"
ln -s "/configfs/vimc/mdev/vimc-capture:cap-sca/sink:0" "/configfs/vimc/mdev/vimc-scaler:sca/source:1/to-cap-sca"
echo immutable > "/configfs/vimc/mdev/vimc-scaler:sca/source:1/to-cap-sca/type"

#sen -> cap-sen
mkdir "/configfs/vimc/mdev/vimc-sensor:sen/source:0/to-cap-sen"
ln -s "/configfs/vimc/mdev/vimc-capture:cap-sen/sink:0" "/configfs/vimc/mdev/vimc-sensor:sen/source:0/to-cap-sen"
echo immutable > "/configfs/vimc/mdev/vimc-sensor:sen/source:0/to-cap-sen/type"

#deb -> cap-deb
mkdir "/configfs/vimc/mdev/vimc-debayer:deb/source:1/to-cap-deb"
ln -s "/configfs/vimc/mdev/vimc-capture:cap-deb/sink:0" "/configfs/vimc/mdev/vimc-debayer:deb/source:1/to-cap-deb"
echo immutable > "/configfs/vimc/mdev/vimc-debayer:deb/source:1/to-cap-deb/type"
