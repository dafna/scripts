#!/bin/bash

# grep -P --byte-offset --only-matching --text "\x00\x00\x00\x01" ~/git/vimc/images/some.h264 | grep '[0-9]*' --text -o > offsets.txt
# remove the first row from offsets.txt


idx=1
pidx=0
pline=0

while IFS= read -r line; do
	echo "==="
	echo $line
	echo $pline
	tail  images/some.h264 -c $(( 37362228 - $pline )) | head -c $(( $line - $pline )) > nal$pidx.txt
	echo $(( $line - $pline ))
	pline=$line
	pidx=$idx
    idx=$((idx + 1))
done < offsets.txt
