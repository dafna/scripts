#!/bin/bash
echo 15 > /proc/sys/kernel/printk
echo "file drivers/media/platform/vimc/* +p" > /sys/kernel/debug/dynamic_debug/control

for ((i=1;i<=100;i++));
do
	echo $i
	modprobe vimc
	echo "file drivers/media/platform/vimc/* +p" > /sys/kernel/debug/dynamic_debug/control
	media-ctl -d platform:vimc -V '"Sensor A":0[fmt:SBGGR8_1X8/640x480]'
	media-ctl -d platform:vimc -V '"Debayer A":0[fmt:SBGGR8_1X8/640x480]'
	media-ctl -d platform:vimc -V '"Sensor B":0[fmt:SBGGR8_1X8/640x480]'
	media-ctl -d platform:vimc -V '"Debayer B":0[fmt:SBGGR8_1X8/640x480]'
	v4l2-ctl -z platform:vimc -d "RGB/YUV Capture" -v width=1920,height=1440
	v4l2-ctl -z platform:vimc -d "Raw Capture 0" -v pixelformat=BA81
	v4l2-ctl -z platform:vimc -d "Raw Capture 1" -v pixelformat=BA81
	v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video2 & sleep 1; echo -n vimc.0 >/sys/bus/platform/drivers/vimc/unbind
	echo -n vimc.0 >/sys/bus/platform/drivers/vimc/bind
	modprobe vimc -r
done

