media-ctl -d platform:vimc -V '"Sensor A":0[fmt:SBGGR8_1X8/800x1000]'
media-ctl -d platform:vimc -V '"Debayer A":0[fmt:SBGGR8_1X8/800x1000]'
media-ctl -d platform:vimc -V '"Sensor B":0[fmt:SBGGR8_1X8/800x1000]'
media-ctl -d platform:vimc -V '"Debayer B":0[fmt:SBGGR8_1X8/800x1000]'

media-ctl -d platform:vimc -V '"Scaler":0[fmt:RGB888_1X24/800x1000]'
media-ctl -d platform:vimc -V '"Scaler":0[crop:(50,70)/750x500]'

v4l2-ctl -z platform:vimc -d "RGB/YUV Capture" -v width=2250,height=1500
v4l2-ctl -z platform:vimc -d "Raw Capture 0" -v pixelformat=BA81,width=800,height=1000
v4l2-ctl -z platform:vimc -d "Raw Capture 1" -v pixelformat=BA81,width=800,height=1000

v4l2-ctl --stream-mmap --stream-count=10 -d /dev/video5
v4l2-ctl --stream-mmap --stream-count=10 -d /dev/video6
v4l2-ctl --stream-mmap --stream-count=10 -d /dev/video7
