#!/bin/bash
# Display commands and their arguments as they are executed.
# set -x

# we set all links to be enabled and immutable
# therefore, when streaming we should make sure that
# so, if trying to stream the sensor capture, we should
# still make sure that also the formats in all other links match

source ./functions.sh

VIDSCA=/dev/video2
VIDSEN=/dev/video1
VIDDEB=/dev/video0
STRM_CNT=10
# generates the v4l-ctl streaming output "<<<<<<" ...
STRM_OUT=$(printf '<%.0s' `seq $STRM_CNT`)

echo 15 > /proc/sys/kernel/printk

test_idx=1

function stream_should_not_success {
	v4l2-ctl --stream-mmap --stream-count=$1 -d $2
	out=$(v4l2-ctl --stream-mmap --stream-count=$1 -d $2 2>&1)
	out="${out//[^<]}"
	if [ "$out" == $STRM_OUT ]; then echo "streaming DID NOT fail (it should have)"; exit; fi
}

function stream_should_success {
	v4l2-ctl --stream-mmap --stream-count=$1 -d $2
	out=$(v4l2-ctl --stream-mmap --stream-count=$1 -d $2 2>&1)
	out="${out//[^<]}"
	if [ "$out" != $STRM_OUT ]; then echo "streaming sen failed $out"; exit; fi
}

function test1 {

reinstall_vimc
simple_topo mdev || exit 1

echo "===================================================================================="
echo "Test 1: make sure that the deivce can be plugged and all captures can stream"
echo "===================================================================================="
((test_idx++))
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
media-ctl -d0 --print-dot > vmpath/simple.dot
media-ctl -d0 --print-dot | dot -Tps -o vmpath/simple.ps

configure_all_formats
stream_should_success $STRM_CNT $VIDSEN
stream_should_success $STRM_CNT $VIDDEB
stream_should_success $STRM_CNT $VIDSCA
}

function test2 {
reinstall_vimc
simple_topo mdev || exit 1
echo "==============================================================================="
echo "Test 2: make sure setting the type works"
echo "==============================================================================="
((test_idx++))
#should not succeed
echo off > "/configfs/vimc/mdev/vimc-debayer:deb/sink:0/sen-to-deb/type" || exit 1
if [ 'd' != $(head -c 1 "/configfs/vimc/mdev/vimc-debayer:deb/sink:0/sen-to-deb/type") ]; then exit; fi;
echo 0 > /configfs/vimc/mdev/hotplug || exit 1
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
media-ctl -d0 --print-dot | dot -Tps -o vmpath/simple-test${test_idx}-d.ps
configure_all_formats
stream_should_not_success $STRM_CNT $VIDSCA

echo on > "/configfs/vimc/mdev/vimc-debayer:deb/sink:0/sen-to-deb/type" || exit 1
if [ 'e' != $(head -c 1 "/configfs/vimc/mdev/vimc-debayer:deb/sink:0/sen-to-deb/type") ]; then exit; fi;
echo 0 > /configfs/vimc/mdev/hotplug || exit 1
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
media-ctl -d0 --print-dot | dot -Tps -o vmpath/simple-test${test_idx}-e.ps
configure_all_formats
stream_should_success $STRM_CNT $VIDDEB
}

function test3 {
reinstall_vimc
simple_topo mdev || exit 1
echo "=========================================================================="
echo "Test 3: make sure no two entities with the same name is allowed"
echo "=========================================================================="
((test_idx++))
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
mkdir "/configfs/vimc/mdev/vimc-scaler:sen" && exit 1
mkdir "/configfs/vimc/mdev/vimc-capture:deb" && exit 1
mkdir "/configfs/vimc/mdev/vimc-sensor:cap-sen" && exit 1
echo 0 > /configfs/vimc/mdev/hotplug || exit 1
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
}
function test4 () {
reinstall_vimc
simple_topo mdev || exit 1
echo "=========================================================================="
echo "Test 4: make sure it is impossible to remove the scaler and the sensor (since they are linked)"
echo "=========================================================================="
((test_idx++))
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
rmdir "/configfs/vimc/mdev/vimc-scaler:sca" && exit 1
rmdir "/configfs/vimc/mdev/vimc-sensor:sen" && exit 1
echo 0 > /configfs/vimc/mdev/hotplug || exit 1
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
}
function test5 {
reinstall_vimc
simple_topo mdev || exit 1
echo "=========================================================================="
echo "Test 5: remove the two links [debayer -> scaler],[scaler->vimc-cpature] and the scaler"
echo "and make sure that the cap-sca can't be streamed and that the the other captures can"
echo "=========================================================================="
((test_idx++))
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
rm /configfs/vimc/mdev/vimc-debayer:deb/source:1/deb-to-sca || exit 1
rm /configfs/vimc/mdev/vimc-scaler:sca/source:1/sca-to-cap || exit 1
rmdir /configfs/vimc/mdev/vimc-scaler:sca/sink:0/* || exit 1
rmdir "/configfs/vimc/mdev/vimc-scaler:sca" || exit 1
echo 0 > /configfs/vimc/mdev/hotplug || exit 1
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
configure_all_formats
stream_should_success $STRM_CNT $VIDDEB
stream_should_success $STRM_CNT $VIDSEN
stream_should_not_success $STRM_CNT $VIDSCA

}
function test6 {
reinstall_vimc
simple_topo mdev || exit 1
echo "=========================================================================="
echo "Test 6: remove the scaler and its links and create it again and make"
echo "sure the device can be plugged and stream"
echo "=========================================================================="
((test_idx++))
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
rm /configfs/vimc/mdev/vimc-debayer:deb/source:1/deb-to-sca || exit 1
rm /configfs/vimc/mdev/vimc-scaler:sca/source:1/sca-to-cap || exit 1
rmdir /configfs/vimc/mdev/vimc-scaler:sca/sink:0/* || exit 1
rmdir "/configfs/vimc/mdev/vimc-scaler:sca" || exit 1

mkdir /configfs/vimc/mdev/vimc-scaler:sca/
mkdir /configfs/vimc/mdev/vimc-scaler:sca/sink:0/deb-to-sca || exit 1
ln -s /configfs/vimc/mdev/vimc-scaler:sca/sink:0/deb-to-sca /configfs/vimc/mdev/vimc-debayer:deb/source:1/deb-to-sca
echo on > /configfs/vimc/mdev/vimc-scaler:sca/sink:0/deb-to-sca/type

ln -s /configfs/vimc/mdev/vimc-capture:cap-sca/sink:0/sca-to-cap /configfs/vimc/mdev/vimc-scaler:sca/source:1/sca-to-cap
echo on > /configfs/vimc/mdev/vimc-capture:cap-sca/sink:0/sca-to-cap/type

echo 0 > /configfs/vimc/mdev/hotplug || exit 1
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
configure_all_formats
stream_should_success $STRM_CNT $VIDSEN
stream_should_success $STRM_CNT $VIDDEB
stream_should_success $STRM_CNT $VIDSCA
}

function test7 {
reinstall_vimc
simple_topo mdev || exit 1
echo "==============================================================================="
echo "Test 7: create two simple devices and make sure that they can both be loaded and upstreamed together"
echo "and also that it is impossible to link entities between them."
echo "==============================================================================="
((test_idx++))
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
simpler_topo mdev2 || exit 1
echo 1 > /configfs/vimc/mdev2/hotplug || exit 1
configure_all_formats
configure_all_formats_simpler
media-ctl -d0 --print-dot | dot -Tps -o vmpath/simple0-test${test_idx}.ps
media-ctl -d1 --print-dot | dot -Tps -o vmpath/simple1-test${test_idx}.ps
stream_should_success $STRM_CNT $VIDSEN
stream_should_success $STRM_CNT $VIDDEB
stream_should_success $STRM_CNT $VIDSCA
stream_should_success $STRM_CNT /dev/video3
mkdir /configfs/vimc/mdev2/vimc-capture:cap/sink:0/deb-from-mdev
ln -s /configfs/vimc/mdev2/vimc-capture:cap/sink:0/deb-from-mdev /configfs/vimc/mdev/vimc-debayer:deb/source:1/ && exit 1
mkdir /configfs/vimc/mdev/vimc-capture:cap-sen/sink:0/sen-from-mdev2
ln -s /configfs/vimc/mdev/vimc-capture:cap-sen/sink:0/sen-from-mdev2 /configfs/vimc/mdev2/vimc-sensor:sen/source:0/ && exit 1

}

function test8 {
reinstall_vimc
simple_topo mdev || exit 1
echo "============================================="
echo "Test 8: unregister the device while streaming"
echo "============================================="
echo 1 > /configfs/vimc/mdev/hotplug || exit 1
media-ctl -d0 --print-dot > vmpath/simple.dot
media-ctl -d0 --print-dot | dot -Tps -o vmpath/simple.ps

configure_all_formats
v4l2-ctl --stream-mmap --stream-count=1000 -d $VIDSEN &
sleep 2
echo 0 > /configfs/vimc/mdev/hotplug
}


function test9 {
	reinstall_vimc
	simple_topo mdev || exit 1
	echo "============================================="
	echo " change the device while streaming"
	echo "============================================="
	echo 1 > /configfs/vimc/mdev/hotplug || exit 1
	media-ctl -d0 --print-dot > vmpath/simple.dot
	media-ctl -d0 --print-dot | dot -Tps -o vmpath/simple.ps

	configure_all_formats
	v4l2-ctl --stream-mmap --stream-count=1000 -d $VIDSEN &
	sleep 2
	mkdir "/configfs/vimc/mdev/vimc-sensor:new-sen" || exit 1
	mkdir "/configfs/vimc/$DEV/vimc-debayer:deb/sink:0/new-sen-to-deb"
	echo off > "/configfs/vimc/$DEV/vimc-debayer:deb/sink:0/sen-to-deb/type"
	ln -s "/configfs/vimc/$DEV/vimc-debayer:deb/sink:0/new-sen-to-deb" "/configfs/vimc/$DEV/vimc-sensor:new-sen/source:0"
	echo 0 > /configfs/vimc/mdev/hotplug
	echo 1 > /configfs/vimc/mdev/hotplug
	media-ctl -d0 --print-dot > vmpath/test9.dot
	rm  vmpath/test9.ps
	media-ctl -d0 --print-dot | dot -Tps -o vmpath/test9.ps
}

function test10 {
	reinstall_vimc
	simple_topo mdev || exit 1
	echo "============================================="
	echo "connect two debayers"
	echo "============================================="
	mkdir "/configfs/vimc/mdev/vimc-debayer:new-deb" || exit 1
	mkdir "/configfs/vimc/$DEV/vimc-debayer:new-deb/sink:0/deb-to-new-deb"
	echo on > "/configfs/vimc/$DEV/vimc-debayer:new-deb/sink:0/deb-to-new-deb/type"
	ln -s "/configfs/vimc/$DEV/vimc-debayer:new-deb/sink:0/deb-to-new-deb" "/configfs/vimc/$DEV/vimc-debayer:deb/source:1"
	mkdir "/configfs/vimc/$DEV/vimc-capture:cap-deb/sink:0/new-deb-to-cap"
	ln -s "/configfs/vimc/$DEV/vimc-capture:cap-deb/sink:0/new-deb-to-cap" "/configfs/vimc/$DEV/vimc-debayer:new-deb/source:1"
	echo off > "/configfs/vimc/$DEV/vimc-capture:cap-deb/sink:0/deb-to-cap/type"
	echo on > "/configfs/vimc/$DEV/vimc-capture:cap-deb/sink:0/new-deb-to-cap/type"
	rm  vmpath/test10.ps
	find /configfs -name "*sca*" -type l -delete
	find /configfs -name "*sca*" -type d -delete
	echo 1 > /configfs/vimc/mdev/hotplug
	media-ctl -d0 --print-dot | dot -Tps -o vmpath/test10.ps
	configure_all_formats
}

function link {
	from=$1
	ftype=$2
	fpad=$3
	to=$4
	ttype=$5
	tpad=$6
	ltype=$7
	mkdir "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}"
	echo $ltype > "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}/type"
	ln -s "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}" "/configfs/vimc/$DEV/${ftype}:${from}/source:${fpad}"
}
function test11 {
	reinstall_vimc
	#simpler_topo mdev || exit 1
	echo "============================================="
	echo "connenct two scalers"
	echo "============================================="

	DEV=mdev
	mkdir "/configfs/vimc/$DEV" || exit 1
	mkdir "/configfs/vimc/mdev/vimc-sensor:sen" || exit 1
	mkdir "/configfs/vimc/mdev/vimc-debayer:deb" || exit 1
	link sen vimc-sensor 0 deb vimc-debayer 0 on

	mkdir "/configfs/vimc/mdev/vimc-scaler:sca1" || exit 1
	link deb vimc-debayer 1 sca1 vimc-scaler 0 on

	mkdir "/configfs/vimc/mdev/vimc-scaler:sca2" || exit 1
	link sca1 vimc-scaler 1 sca2 vimc-scaler 0 on

	mkdir "/configfs/vimc/mdev/vimc-capture:cap" || exit 1
	link sca2 vimc-scaler 1 cap vimc-capture 0 on
	echo 1 > /configfs/vimc/mdev/hotplug
	media-ctl -d0 --print-dot | dot -Tps -o vmpath/test11.ps
	W=20
	H=20
	W3=60
	H3=60
	W9=180
	H9=180
	SEN_0="\"sen\":0[fmt:SBGGR8_1X8/${W}x${H}]"
	DEB_0="\"deb\":0[fmt:SBGGR8_1X8/${W}x${H}]"
	DEB_1="\"deb\":1[fmt:RGB888_1X24/${W}x${H}]"
	SCA1_0="\"sca1\":0[fmt:SBGGR8_1X8/${W}x${H}]"
	#SCA1_1=""sca1":1[fmt:SBGGR8_1X8/${W}x${H}]"
	SCA2_0="\"sca2\":0[fmt:SBGGR8_1X8/${W3}x${H3}]"
	#SCA2_1='"sca2":1[fmt:SBGGR8_1X8/${W}x${H}]"

	media-ctl -d platform:vimc-000 -V "${SEN_0},${DEB_0},${DEB_1},${SCA1_0},${SCA2_0}"
	#v4l2-ctl -z platform:vimc-000 -d "cap-sen" -v pixelformat=BA81
	#v4l2-ctl -z platform:vimc-000 -d "cap-deb" -v pixelformat=RGB3
	v4l2-ctl -z platform:vimc-000 -d "cap" -v pixelformat=RGB3,width=${W9},height=${H9}
}
function test12 {
	reinstall_vimc
	modprobe -r vimc
	modprobe -v vimc sca_mult=1
	echo "============================================="
	echo "add pipeline longer than VIMC_STREAMER_PIPELINE_MAX_SIZE 16"
	echo "============================================="
	
	DEV=mdev
	mkdir "/configfs/vimc/$DEV" || exit 1
	mkdir "/configfs/vimc/mdev/vimc-sensor:sen" || exit 1
	mkdir "/configfs/vimc/mdev/vimc-debayer:deb" || exit 1
	link sen vimc-sensor 0 deb vimc-debayer 0 on

	mkdir "/configfs/vimc/mdev/vimc-scaler:sca1" || exit 1
	link deb vimc-debayer 1 sca1 vimc-scaler 0 on

	for ((i=2;i<=16;i++));
	do
		j=$(($i - 1))
		mkdir "/configfs/vimc/mdev/vimc-scaler:sca${i}" || exit 1
		link sca$j vimc-scaler 1 sca$i vimc-scaler 0 on
	done

	mkdir "/configfs/vimc/mdev/vimc-capture:cap" || exit 1
	link sca16 vimc-scaler 1 cap vimc-capture 0 on
	echo 1 > /configfs/vimc/mdev/hotplug
	media-ctl -d0 --print-dot | dot -Tps -o vmpath/test12.ps
	W=180
	H=180
	SEN_0="\"sen\":0[fmt:SBGGR8_1X8/${W}x${H}]"
	DEB_0="\"deb\":0[fmt:SBGGR8_1X8/${W}x${H}]"
	DEB_1="\"deb\":1[fmt:RGB888_1X24/${W}x${H}]"
	media-ctl -d platform:vimc-000 -V "${SEN_0},${DEB_0},${DEB_1}"
	for ((i=1;i<=16;i++));
	do
		media-ctl -d platform:vimc-000 -V "\"sca${i}\":0[fmt:RGB888_1X24/${W}x${H}]"
	done
	v4l2-ctl -z platform:vimc-000 -d "cap" -v pixelformat=RGB3,width=${W},height=${H}
	v4l2-ctl --stream-mmap --stream-count=$1 -d0
}

function test13 {
	reinstall_vimc
	echo "============================================="
	echo "stream for several pipelines from several devices"
	echo "create 10 sensors connected to a debayer connected to a scaler"
	echo "and try to stream from all sensors and from the scaler"
	echo "============================================="
	DEV=mdev

	mkdir /configfs/vimc/$DEV/
	mkdir /configfs/vimc/$DEV/vimc-scaler:sca
	for ((i=1;i<=16;i++));
	do
		j=$(($i - 1))
		mkdir "/configfs/vimc/mdev/vimc-sensor:sen${i}" || exit 1
		mkdir "/configfs/vimc/mdev/vimc-capture:cap${i}" || exit 1
		link sen$i vimc-sensor 0 sca vimc-scaler 0 off
		link sen$i vimc-sensor 0 cap$i vimc-capture 0 on
	done
	echo 1 > /configfs/vimc/mdev/hotplug
	media-ctl -d0 --print-dot | dot -Tps -o vmpath/test13.ps
	media-ctl -d platform:vimc-000 -V "\"sca\":0[fmt:RGB888_1X24/300x300]"
	for ((i=1;i<=16;i++));
	do
		echo "==$i=="
		media-ctl -d platform:vimc-000 -V "\"sen${i}\":0[fmt:RGB888_1X24/100x100]"
		v4l2-ctl -z platform:vimc-000 -d "cap${i}" -v pixelformat=RGB3,width=100,height=100
	done
	for ((i=1;i<=16;i++));
	do
		echo "==$i=="
		v4l2-ctl -z platform:vimc-000 -d "cap${i}" --stream-mmap --stream-count=50 &
	done
	sleep 1
	echo 0 > /configfs/vimc/mdev/hotplug
}

#- test concurency
#echo "============================================="
#}

function test14 {
	reinstall_vimc
	echo "============================================="
	echo "create more than 64 devices and make sure it is not possible"
	echo "then, remove a random device and add a device and make sure it is possible"
	echo "============================================="
	for ((i=1;i<=65;i++));
	do
		echo "==$i=="
		mkdir "/configfs/vimc/mdev${i}/" || exit 1
		mkdir "/configfs/vimc/mdev${i}/vimc-sensor:sen" || exit 1
		mkdir "/configfs/vimc/mdev${i}/vimc-capture:cap" || exit 1
		echo 1 > "/configfs/vimc/mdev$i/hotplug"
		#link sen$i vimc-sensor 0 sca vimc-scaler 0 off
		#link sen$i vimc-sensor 0 cap$i vimc-capture 0 on
	done
	j=65
	echo 0 > "/configfs/vimc/mdev65/hotplug"
	for ((i=1;i<=64;i++));
	do
		echo "==$i=="
		echo 0 > "/configfs/vimc/mdev$i/hotplug"
		echo 1 > "/configfs/vimc/mdev$j/hotplug"
		j=$(($i))
		#link sen$i vimc-sensor 0 sca vimc-scaler 0 off
		#link sen$i vimc-sensor 0 cap$i vimc-capture 0 on
	done
	echo 1 > "/configfs/vimc/mdev64/hotplug"

}
function test15 {
	reinstall_vimc
	modprobe -r vimc
	modprobe -v vimc sca_mult=1
	echo "============================================="
	echo "create a device with a circle"
	echo "then, remove a random device and add a device and make sure it is possible"
	echo "============================================="

	DEV=mdev
	mkdir "/configfs/vimc/mdev/" || exit 1
	mkdir "/configfs/vimc/mdev/vimc-scaler:sca1" || exit 1
	mkdir "/configfs/vimc/mdev/vimc-scaler:sca2" || exit 1
	mkdir "/configfs/vimc/mdev/vimc-capture:cap" || exit 1
	link sca1 vimc-scaler 1 sca2 vimc-scaler 0 on
	link sca2 vimc-scaler 1 sca1 vimc-scaler 0 on
	link sca2 vimc-scaler 1 cap vimc-capture 0 on
	echo 1 > /configfs/vimc/mdev/hotplug
	media-ctl -d0 --print-dot | dot -Tps -o vmpath/test15.ps
	media-ctl -d platform:vimc-000 -V "\"sca1\":0[fmt:RGB888_1X24/100x100]"
	media-ctl -d platform:vimc-000 -V "\"sca2\":0[fmt:RGB888_1X24/100x100]"
	v4l2-ctl -z platform:vimc-000 -d "cap" -v pixelformat=RGB3,width=100,height=100
	v4l2-ctl -z platform:vimc-000 -d "cap" --stream-mmap --stream-count=10

}

function test16 {
	reinstall_vimc
		modprobe -r vimc
		modprobe -v vimc sca_mult=1
		echo "============================================="
		echo "make sure it is impossible to create a link from an entity to itself"
		echo "============================================="

		DEV=mdev
		mkdir "/configfs/vimc/mdev/" || exit 1
		mkdir "/configfs/vimc/mdev/vimc-scaler:sca1" || exit 1
		link sca1 vimc-scaler 1 sca1 vimc-scaler 0 on && echo "Failure: connecting a scaler to itself should not be allowed" && exit 1

}


if [ $# == 0 ]; then
	for test_idx in {1..16}
	do

		test$test_idx
	done
else
	test${1}
fi
exit

