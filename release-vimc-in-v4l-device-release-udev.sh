i=0
while [ 1 ]; do
	echo "=== $i ==="
	modprobe vimc
	media-ctl -d platform:vimc -V '"Sensor A":0[fmt:SBGGR8_1X8/640x480],"Debayer A":0[fmt:SBGGR8_1X8/640x480]'
	media-ctl -d platform:vimc -V '"Sensor B":0[fmt:SBGGR8_1X8/640x480],"Debayer B":0[fmt:SBGGR8_1X8/640x480]'
	v4l2-ctl -d2 -v width=1920,height=1440
	v4l2-ctl -d0 -v pixelformat=BA81
	v4l2-ctl -d1 -v pixelformat=BA81
	v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video2 &
	v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video1 &
	sleep 0.6
	echo -n vimc.0 >/sys/bus/platform/drivers/vimc/unbind
	sleep 0.1
	modprobe -r vimc
	i=$((i + 1))
done

