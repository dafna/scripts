i=0
while [ 1 ]; do
	echo "=== $i ==="
	modprobe vimc
	# dislink 'Debyer A' from the scaler
	media-ctl -d platform:vimc -l "5:1->21:0[0]" -v
	# link 'RGB/YUV Input' to the scaler
	media-ctl -d platform:vimc -l "19:0->21:0[1]" -v
	media-ctl -d platform:vimc -V '"Sensor A":0[fmt:SBGGR8_1X8/640x480],"Debayer A":0[fmt:SBGGR8_1X8/640x480]'
	media-ctl -d platform:vimc -V '"Sensor B":0[fmt:SBGGR8_1X8/640x480],"Debayer B":0[fmt:SBGGR8_1X8/640x480]'
	media-ctl -d platform:vimc -V '"RGB/YUV Input":0[fmt:SBGGR8_1X8/640x480]'
	media-ctl -d platform:vimc -V '"Scaler":0[fmt:SBGGR8_1X8/640x480]'
	v4l2-ctl -d2 -v width=1920,height=1440,pixelformat=BA81
	v4l2-ctl -d0 -v pixelformat=BA81
	v4l2-ctl -d1 -v pixelformat=BA81
	v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video2 & #no work yet
	#v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video1 &
	#v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video0 &
	exit
	sleep 0.6
	echo -n vimc.0 >/sys/bus/platform/drivers/vimc/unbind
	sleep 0.1
	modprobe -r vimc
	i=$((i + 1))
done

