# This script tests vivid for unbinding the device while it streams.
# enable CONFIG_KASAN to get a report of bugs that it might catch.


modprobe vivid vivid_debug=1
echo -n vivid.0 >/sys/bus/platform/drivers/vivid/bind
#./bla.sh &
while [ 1 ]; do
#media-ctl -d platform:vivid -V '"Sensor A":0[fmt:SBGGR8_1X8/640x480],"Debayer A":0[fmt:SBGGR8_1X8/640x480]'

#v4l2-ctl -d2 -v width=1920,height=1440
#v4l2-ctl -d5 -v pixelformat=BA81
#v4l2-ctl -d6 -v pixelformat=BA81
#v4l2-ctl -d7 -v pixelformat=BA81
v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video0 &
sleep 1.70
echo -n vivid.0 >/sys/bus/platform/drivers/vivid/unbind
sleep 0.1
echo -n vivid.0 >/sys/bus/platform/drivers/vivid/bind
sleep 0.1
done
