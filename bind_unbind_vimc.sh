#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
# Find device number in /sys/bus/usb/drivers/drivername
# Edit this file to update the driver numer and name
# Example test for uvcvideo driver
function bind_unbind_vimc {
	i=0
	while :; do
	  i=$((i+1))
	  echo vimc.0 > /sys/bus/platform/drivers/vimc/unbind;
	  echo vimc.0 > /sys/bus/platform/drivers/vimc/bind;
	  sleep 3
	  #clear
	  #echo $i
	done
}

bind_unbind_vimc
#./media_device_test -d /dev/media0 &

