#!/bin/bash
source ./functions.sh

while true
do
	reinstall_vimc
	simple_topo mdev$(( ( RANDOM % 3 )  + 1 ))
	echo 1 > /configfs/vimc/mdev/hotplug
	sleep $(( ( RANDOM % 3 )  + 1 ))
done

