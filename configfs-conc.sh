#!/bin/bash

ENT_NUM=50
DEV=mdev
function plug_unplug {
	k=0
	while :
#for k in  `seq 1 1000000`
	do
		#echo "K is $k"
		echo 1 > /configfs/vimc/$DEV/hotplug
		#sleep $((RANDOM % 5))
		echo 0 > /configfs/vimc/$DEV/hotplug
		#sleep $((RANDOM % 2))
		#echo 1 > /configfs/vimc/$DEV/hotplug
		k=$(($k + 1))
	done
}

function create_entities {
while :
#	for k in  `seq 1 ${BIS}`
	do
		for i in `seq 1 ${ENT_NUM}`
		do
			mkdir -p /configfs/vimc/$DEV/vimc-sensor:sen$i
			mkdir -p /configfs/vimc/$DEV/vimc-debayer:deb$i
			mkdir -p /configfs/vimc/$DEV/vimc-scaler:sca$i
			#mkdir -p /configfs/vimc/$DEV/vimc-capture:cap$i
		done
		j=$((RANDOM % 3))
		sleep 1
	done
}

function link {
	from=$1
		ftype=$2
		fpad=$3
		to=$4
		ttype=$5
		tpad=$6
		ltype=$7
		mkdir -p "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}"
		echo $ltype > "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}/type"
		ln -s "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}" "/configfs/vimc/$DEV/${ftype}:${from}/source:${fpad}" 2>/dev/null #&& echo "link ${from}->${to}"
}

#3 - one script links between entities
function create_links {
#for k in  `seq 1 ${BIS}`
	while :
#for k in  `seq 1 10`
	do
		r1=$((RANDOM % 50))
		r2=$((RANDOM % 50))
		link sen$r1 vimc-sensor 0 deb$r2 vimc-debayer 0 on
		r1=$((RANDOM % 50))
		r2=$((RANDOM % 50))
		#link sen$r1 vimc-sensor 0 cap$r2 vimc-capture 0 on
		r1=$((RANDOM % 50))
		r2=$((RANDOM % 50))
		link deb$r1 vimc-debayer 1 sca$r2 vimc-scaler 0 on
		r1=$((RANDOM % 50))
		r2=$((RANDOM % 50))
		#link sca$r1 vimc-scaler 1 cap$r2 vimc-capture 0 on
		r1=$((RANDOM % 50))
		r2=$((RANDOM % 50))
		link sca$r1 vimc-scaler 1 sca$r2 vimc-scaler 0 on
	done
}

#one script delet entities
function delete_links_and_entities {
	while :
#	for k in  `seq 1 ${BIS}`
	do
		find /configfs -type l -name "sen*" -delete && echo "OK sen links"
		find /configfs -type d -name "vimc-sensor*" -delete && echo "OK sen"
		#sleep $((RANDOM % 2))

		find /configfs -type l -name "deb*" -delete && echo "OK deb links"
		find /configfs -type d -name "*to-deb*" -delete && echo "OK deb target"
		find /configfs -type d -name "vimc-debayer*" -delete && echo "OK deb"
		#sleep $((RANDOM % 2))

		find /configfs -type l -name "sca*" -delete && echo "OK sca links"
		find /configfs -type d -name "*to-sca*" -delete && echo "OK sca target"
		find /configfs -type d -name "vimc-scaler*" -delete && echo "OK sca"
		#sleep $((RANDOM % 2))

		#find /configfs -type d -name "*to-cap*" -delete && echo "OK cap target"
		#find /configfs -type d -name "vimc-capture*" -delete && echo "OK cap"
		sleep $((RANDOM % 2))
	done
}

function main {
	modprobe vimc || exit 1
	mount -t configfs none /configfs
	echo vimc* > /sys/kernel/debug/tracing/set_ftrace_filter
	echo > /sys/kernel/debug/tracing/trace

	echo 15 > /proc/sys/kernel/printk
	create_entities &
	create_links &
	delete_links_and_entities &
	plug_unplug &
}
main
