# This script tests vimc for unbinding the device while it streams.
# enable CONFIG_KASAN to get a report of bugs that it might catch.

modprobe vimc sca_mult=1
echo -n vimc.0 >/sys/bus/platform/drivers/vimc/bind
while [ 1 ]; do
media-ctl -d platform:vimc -V '"Sensor A":0[fmt:SBGGR8_1X8/640x480],"Debayer A":0[fmt:SBGGR8_1X8/640x480]'

#v4l2-ctl -d2 -v width=1920,height=1440
v4l2-ctl -d5 -v pixelformat=BA81
v4l2-ctl -d6 -v pixelformat=BA81
#v4l2-ctl -d7 -v pixelformat=BA81
v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video7 &
sleep 0.20
echo -n vimc.0 >/sys/bus/platform/drivers/vimc/unbind
sleep 0.1
echo -n vimc.0 >/sys/bus/platform/drivers/vimc/bind
sleep 0.1
done
