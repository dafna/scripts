#!/bin/bash
echo "start simple topo"
# Creating the entities
mkdir "/configfs/vimc/mdev"
mkdir "/configfs/vimc/mdev/vimc-sensor:sen"
mkdir "/configfs/vimc/mdev/vimc-debayer:deb"
mkdir "/configfs/vimc/mdev/vimc-scaler:sca"
mkdir "/configfs/vimc/mdev/vimc-capture:cap-sca" #/dev/video2
mkdir "/configfs/vimc/mdev/vimc-capture:cap-sen" #/dev/video1
mkdir "/configfs/vimc/mdev/vimc-capture:cap-deb" #/dev/video0

# Creating the links
#sen -> deb
mkdir "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-deb"
ln -s "/configfs/vimc/mdev/vimc-debayer:deb/pad:sink:0" "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-deb"
echo on > "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-deb/enabled"
echo on > "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-deb/immutable"

#deb -> sca
mkdir "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-sca"
ln -s "/configfs/vimc/mdev/vimc-scaler:sca/pad:sink:0" "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-sca"
echo on > "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-sca/enabled"
echo on > "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-sca/immutable"

#sca -> cap-sca
mkdir "/configfs/vimc/mdev/vimc-scaler:sca/pad:source:1/to-cap"
ln -s "/configfs/vimc/mdev/vimc-capture:cap-sca/pad:sink:0" "/configfs/vimc/mdev/vimc-scaler:sca/pad:source:1/to-cap"
echo on > "/configfs/vimc/mdev/vimc-scaler:sca/pad:source:1/to-cap/enabled"
echo on > "/configfs/vimc/mdev/vimc-scaler:sca/pad:source:1/to-cap/immutable"

#sen -> cap-sen
mkdir "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-cap"
ln -s "/configfs/vimc/mdev/vimc-capture:cap-sen/pad:sink:0" "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-cap"
echo on > "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-cap/enabled"
echo on > "/configfs/vimc/mdev/vimc-sensor:sen/pad:source:0/to-cap/immutable"

#deb -> cap-deb
mkdir "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-cap"
ln -s "/configfs/vimc/mdev/vimc-capture:cap-deb/pad:sink:0" "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-cap"
echo on > "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-cap/enabled"
echo on > "/configfs/vimc/mdev/vimc-debayer:deb/pad:source:1/to-cap/immutable"
