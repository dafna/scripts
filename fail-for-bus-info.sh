#!/bin/bash
set -x

modprobe -v vimc
echo "file drivers/media/platform/vimc/* +p" > /sys/kernel/debug/dynamic_debug/control
echo 15 > /proc/sys/kernel/printk

mount -t configfs none /configfs
mkdir /configfs/vimc/mdev
mkdir "/configfs/vimc/mdev/entities/vimc-capture:cap"
echo 1 > /configfs/vimc/mdev/hotplug
v4l2-ctl -d0 --all | grep platform
rmdir "/configfs/vimc/mdev/entities/vimc-capture:cap"
rmdir "/configfs/vimc/mdev/"
modprobe -r vimc
modprobe -v vimc
mkdir /configfs/vimc/mdev
mkdir "/configfs/vimc/mdev/entities/vimc-capture:cap"
echo 1 > /configfs/vimc/mdev/hotplug
v4l2-ctl -d0 --all | grep platform
rmdir "/configfs/vimc/mdev/entities/vimc-capture:cap"
rmdir "/configfs/vimc/mdev/"
modprobe -r vimc


