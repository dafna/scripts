#!/bin/bash

#one script plug and unplug the device

DEV=mdev

BIS=3
ENT_NUM=50

function plug_unplug {
	k=0
	while :
	#for k in  `seq 1 1000000`
	do
		echo "K is $k"
		echo 1 > /configfs/vimc/$DEV/hotplug
		sleep $((RANDOM % 5))
		echo 0 > /configfs/vimc/$DEV/hotplug
		sleep $((RANDOM % 2))
		echo 1 > /configfs/vimc/$DEV/hotplug
		k=$(($k + 1))
	done
}

#one script creates new entities
function create_entities {
	while :
#	for k in  `seq 1 ${BIS}`
	do
		for i in `seq 1 ${ENT_NUM}`
		do
			mkdir -p /configfs/vimc/$DEV/vimc-sensor:sen$i
			mkdir -p /configfs/vimc/$DEV/vimc-debayer:deb$i
			mkdir -p /configfs/vimc/$DEV/vimc-scaler:sca$i
			mkdir -p /configfs/vimc/$DEV/vimc-capture:cap$i
		done
		sleep 1
	done
}

function link {
	from=$1
	ftype=$2
	fpad=$3
	to=$4
	ttype=$5
	tpad=$6
	ltype=$7
	mkdir -p "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}"
	echo $ltype > "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}/type"
	ln -s "/configfs/vimc/$DEV/${ttype}:${to}/sink:${tpad}/${from}-to-${to}" "/configfs/vimc/$DEV/${ftype}:${from}/source:${fpad}" 2>/dev/null
	#ls -l "/configfs/vimc/$DEV/${ftype}:${from}/source:${fpad}/${from}-to-${to}"
}

#3 - one script links between entities
function create_links {
	while :
	do
	for k in  `seq 1 5`
	#for k in  `seq 1 10`
	do
		i=$((RANDOM % 10))
		j=$((RANDOM % 10))
		link sen$i vimc-sensor 0 deb$j vimc-debayer 0 on
		i=$((RANDOM % 10))
		j=$((RANDOM % 10))
		link sen$i vimc-sensor 0 cap$j vimc-capture 0 on
		i=$((RANDOM % 10))
		j=$((RANDOM % 10))
		link deb$i vimc-debayer 1 sca$j vimc-scaler 0 on
		i=$((RANDOM % 10))
		j=$((RANDOM % 10))
		link sca$i vimc-scaler 1 cap$j vimc-capture 0 on
		i=$((RANDOM % 10))
		j=$((RANDOM % 10))
		link sca$i vimc-scaler 1 sca$j vimc-scaler 0 on
		#sleep 2
	done
	for k in  `seq 11 50`
	do
		link sen$k vimc-sensor 0 cap$k vimc-capture 0 on

	done
	sleep 1
	done
}

#one script delet entities
function delete_links_and_entities {
	while :
#	for k in  `seq 1 ${BIS}`
	do
		find /configfs -type l -name "sen*" -delete;
		find /configfs -type d -name "vimc-sensor*" -delete

		find /configfs -type l -name "deb*" -delete;
		find /configfs -type d -name "*to-deb*" -delete && echo "OK deb target"
		find /configfs -type d -name "vimc-debayer*" -delete

		find /configfs -type l -name "sca*" -delete;
		find /configfs -type d -name "*to-sca*" -delete && echo "OK sca target"
		find /configfs -type d -name "vimc-scaler*" -delete

		find /configfs -type d -name "*to-cap*" -delete && echo "OK cap target"
		find /configfs -type d -name "vimc-capture*" -delete
		sleep $((RANDOM % 30))
	done
}
function configure_all_formats {
	SCA_1='"sca":1[fmt:RGB888_1X24/640x480]'

#	media-ctl -d platform:vimc-000 -V '"sen":0[fmt:SBGGR8_1X8/640x480],"deb":0[fmt:SBGGR8_1X8/640x480]'
# media-ctl -d platform:vimc-000 -V '"deb":0[fmt:SBGGR8_1X8/640x480]'
# This is actually the default and the only supported format for deb:1
# see `v4l2-ctl -d /dev/v4l-subdev1 --list-subdev-mbus 1`
#	media-ctl -d platform:vimc-000 -V '"deb":1[fmt:RGB888_1X24/640x480]'
#	media-ctl -d platform:vimc-000 -V '"sca":0[fmt:RGB888_1X24/640x480]'
#	media-ctl -d platform:vimc-000 -V '"sca":1[fmt:RGB888_1X24/640x480]'
	media-ctl -d platform:vimc-000 -V "${SEN_0},${DEB_0},${DEB_1},${SCA_0},${SCA_1}"
	v4l2-ctl -z platform:vimc-000 -d "cap-sen" -v pixelformat=BA81
	v4l2-ctl -z platform:vimc-000 -d "cap-deb" -v pixelformat=RGB3
#The scaler scales times 3, so need to set its capture accordingly
	v4l2-ctl -z platform:vimc-000 -d "cap-sca" -v pixelformat=RGB3,width=1920,height=1440
}


#one script configure the device and streams it
function stream {
	while :
	#for k in  `seq 1 10000`
	do
		for i in `seq 11 ${ENT_NUM}`
		do
			for j in {0..0}
			do
				#if [ -c "/dev/media0" ]; then
					#media-ctl -d platform:vimc-00$j -V "\"sen$i\":0[fmt:SBGGR8_1X8/640x480]"
					#media-ctl -d platform:vimc-00$j -V "\"deb$i\":0[fmt:SBGGR8_1X8/640x480]"
					#media-ctl -d platform:vimc-00$j -V "\"sca$i\":0[fmt:RGB888_1X24/640x480]"
					# v4l2-ctl -z platform:vimc-00$j -d "cap$i" -v pixelformat=RGB3
					echo "steaming $i"
					#v4l2-ctl -z platform:vimc-00$j -d "cap$i" -v pixelformat=BA81
					v4l2-ctl -z platform:vimc-00$j -d "cap$i" --stream-mmap --stream-count=10
				#else
				#	echo "no media"
				#fi
			done
		done
		sleep $((RANDOM % 2))
	done
}

modprobe -vr vimc
modprobe -v vimc sca_mult=1
mount -t configfs none /configfs
echo 15 > /proc/sys/kernel/printk
#echo "file drivers/media/platform/vimc/* +p" > /sys/kernel/debug/dynamic_debug/control
#echo "file drivers/media/platform/vimc/vimc-core.c +p" > /sys/kernel/debug/dynamic_debug/control
echo "file drivers/media/platform/vimc/vimc-debayer.c +p" > /sys/kernel/debug/dynamic_debug/control
#echo "file drivers/media/platform/vimc/vimc-configfs.c +p" > /sys/kernel/debug/dynamic_debug/control
#echo 1 > /sys/module/devres/parameters/log
#echo "file drivers/base/core.c +p" > /sys/kernel/debug/dynamic_debug/control
#echo "file drivers/base/dd.c +p" > /sys/kernel/debug/dynamic_debug/control
mkdir /configfs/vimc/$DEV
#plug_unplug &
#create_entities &
#for i in {1..50}
#do
#	mkdir -p /configfs/vimc/$DEV/vimc-sensor:sen$i
#	mkdir -p /configfs/vimc/$DEV/vimc-debayer:deb$i
#	mkdir -p /configfs/vimc/$DEV/vimc-scaler:sca$i
#	mkdir -p /configfs/vimc/$DEV/vimc-capture:cap$i
#done
#while :
#do
echo 0 > /configfs/vimc/$DEV/hotplug
echo 1 > /configfs/vimc/$DEV/hotplug
#done
#link &
#while :
#for i in {1..100}
#do
#	echo $i
#	i=$((RANDOM % ${ENT_NUM}))
#	j=$((RANDOM % ${ENT_NUM}))
#	k=$((RANDOM % ${ENT_NUM}))
#	a=$((RANDOM % ${ENT_NUM}))
#	b=$((RANDOM % ${ENT_NUM}))
#	c=$((RANDOM % ${ENT_NUM}))
#	d=$((RANDOM % ${ENT_NUM}))
#	e=$((RANDOM % ${ENT_NUM}))
#	link sen$i vimc-sensor 0 deb$j vimc-debayer 0 on
#	link deb$k vimc-debayer 1 sca$c vimc-scaler 0 on
#	link sca$a vimc-scaler 1 cap$b vimc-capture 0 on
#	if [ $d == $e ]; then
#		echo "$d == $e"
#	else
#		link sca$d vimc-scaler 1 sca$e vimc-scaler 0 on
#	fi
#	#sleep 2
#done

plug_unplug &
create_entities &
create_links &
delete_links_and_entities &
#delete_links &
#delete_entities &
#create_entities &
stream &
stream &

#echo 1 > /configfs/vimc/$DEV/hotplug
#sleep 2
#media-ctl -d0 --print-dot | dot -Tps -o vmpath/conc.ps
#sleep 3
#echo 0 > /configfs/vimc/$DEV/hotplug
#create_links &
#exit
#delete_links &
#delete_entities &
#stream &


