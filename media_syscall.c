#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/sysmacros.h>
#include <dirent.h>

#include <linux/media.h>

//#include <media-info.h>

int main() {

	int fd = open("/dev/media0", O_RDWR);
	struct media_v2_topology topology;
	struct media_v2_entity *ents = NULL;
	struct media_v2_link *links = NULL;
	struct media_v2_interface *ifaces = NULL;

	if (fd == -1) {
		printf("err open media\n");
		return -1;
	}
	for (int i =0; i<1000000000; i++) {
		memset(&topology, 0, sizeof(topology));
		if (ioctl(fd, MEDIA_IOC_G_TOPOLOGY, &topology)) {
			perror("topology 1 err\n");
			printf("iteration %d\n", i);
			return -1;
		}
		if (!ents) {
			ents = malloc(sizeof(*ents) * topology.num_entities);
			links = malloc(sizeof(*links) * topology.num_links);
			ifaces = malloc(sizeof(*ifaces) * topology.num_interfaces);
		}
		topology.ptr_entities = (__u64)ents;
		topology.ptr_links = (__u64)links;
		topology.ptr_interfaces = (__u64)ifaces;

		if (ioctl(fd, MEDIA_IOC_G_TOPOLOGY, &topology)) {
			perror("topology 2 err\n");
			printf("iteration %d\n", i);
			return -1;
		}
	}

	return 0;
}
